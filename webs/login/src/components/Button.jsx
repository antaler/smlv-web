import { useState } from 'preact/hooks'
import './Button.sass'
export function ButtonLogin ({ children }) {
  const [isLoading, setIsLoading] = useState(false)
  const onClick = async () => {
    const { value: username } = document.getElementById('username')
    const { value: password } = document.getElementById('password')
    if (!username || !password) {
      return
    }
    document.getElementById('username').value = ''
    document.getElementById('password').value = ''
    setIsLoading(true)
    console.log({ username, password })
  }
  if (isLoading) {
    return (
      <div className=' loader w-full h-10 flex flex-row items-center justify-center gap-5'>
        <div className='w-7 h-7 rounded-full bg-accent' />
        <div className='w-7 h-7 rounded-full bg-accent' />
        <div className='w-7 h-7 rounded-full bg-accent' />
      </div>
    )
  }
  return (
    <button
      disabled={isLoading}
      className=' w-1/2 text-xl shadow-md rounded-lg px-4 py-2 bg-primary text-white'
      type='button' onClick={onClick}
    >{children}
    </button>
  )
}
