const URL_LOGIN = 'http://localhost:3000/api/login'

export async function login ({ username, password }) {
  const body = JSON.stringify({ username, password })
  return await fetch(URL_LOGIN, {
    method: 'POST',
    body
  }).then(res => {
    if (res.status === 200) {
      return {
        isOk: true,
        token: res.headers.get('Authorization')
      }
    } else {
      throw new Error('Login KO')
    }
  }).catch(() => {
    return {
      isOk: false,
      token: ''
    }
  })
}
